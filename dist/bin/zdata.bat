ECHO OFF

REM SET JAVA_HOME=D:\SDK\jdk8_x64

SET HOME=..
SET JAVA_OPTS=-server -Denv=local -Dfile.encoding=UTF-8 -Xms64m -Xmx1024m -XX:+UseParallelGC
SET MAIN_CLASS=io.zbus.data.server.DataServer

SET LIB_OPTS=%HOME%;%HOME%/lib/*;%HOME%/classes;%HOME%/*;%HOME%/conf;
IF NOT EXIST "%JAVA_HOME%" (
    SET JAVA=java
) ELSE (
    SET JAVA="%JAVA_HOME%\bin\java"
)
%JAVA% %JAVA_OPTS% -cp %LIB_OPTS% %MAIN_CLASS% %MAIN_OPTS% 
