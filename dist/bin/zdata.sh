#/usr/bin
if [ -z ${JAVA_HOME} ]; then
JAVA_HOME=/apps/jdk
fi
HOME=../
JAVA_OPTS="-Dfile.encoding=UTF-8 -server -Denv=local -Xms64m -Xmx1024m -XX:+UseParallelGC"
MAIN_CLASS=io.zbus.data.server.DataServer

LIB_OPTS="$HOME:$HOME/lib/*:$HOME/classes:$HOME/*:$HOME/conf/"
nohup $JAVA_HOME/bin/java $JAVA_OPTS -cp $LIB_OPTS $MAIN_CLASS $MAIN_OPTS > /dev/null 2>&1&


