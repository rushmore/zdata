
function queryOne(ctx) {
    var db = ctx.db; 
    var res = db.queryOne("select * from company where id=?", [2]);   

    return res;
}

function queryList(ctx) {
    var db = ctx.db; 
    var res = db.queryList("select * from company where id>?", [4]);   

    return res;
}

function queryPage(ctx) { 
    var db = ctx.db; 
    var params = ctx.request.params;
    
    var resp = ctx.response;
    if(!params.p) params.p = 0;
    if(!params.n) params.n = 2; 

    var res = db.queryPage("select * from user", params.p, params.n);   

    return res;
}

function count(ctx) { 
    var db = ctx.db; 
    
    var res = db.queryObject("select count(0) from company");  

    return res;
}

function select(ctx) { 
    var db = ctx.db; 
    
    var res = db.select("user");  //select with table name

    return res;
}

function select1(ctx) {
    var db = ctx.db; 
    
    var where = { 
    	name: {'c': 'jack'}
    };
    
    var data = db.select("user", where);  
    return data;
}

function where(ctx) {
    var db = ctx.db; 
    
    var where = { 
        or: {
            age: {'<': 36},
            name: {'c': 'jack'}
        } 
    };
    
    var data = db.select("user", where);  
    return data;
}



function where2(ctx) {
    var db = ctx.db; 
    
    var where = { 
    	age: {'>': 36}
    };
    
    var data = db.queryList("select * from user", where);  
    return data;
}
 
function where3(ctx) {
    var db = ctx.db; 
    
    var where = { 
    	age: {'>': 36}
    };
    
    var data = db.queryPage("select * from user", 0, 1, where);  
    return data;
}

function where4(ctx) {
    var db = ctx.db; 
    
    var where = { 
    	age: {'>': 36}
    };
    
    var data = db.queryObject("select count(0) from user", where);  
    return data;
}

function where5(ctx) {
    var db = ctx.db; 
    
    var where = { 
    	age: {'>': 36}
    };
    
    var data = db.update("user", {age: 36+1}, where);  
    return data;
}

function where6(ctx) {
    var db = ctx.db; 
    
    var where = { 
    	age: {'>': 36}
    };
    
    var data = db.execute("update user set age=?", [12], where);  
    return data;
}
 