
function test(ctx) {
    const db = ctx.db.use("test"); 
    db.tx(()=>{ 
        db.insert('user', {name: 'rush', age: 12});
        throw Error('to rollback');
    });
    return 'ok';
} 