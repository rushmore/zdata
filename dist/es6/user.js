
function main(ctx) {
    var db = ctx.db; 
    return db.queryList("select * from user");    
}
 
function find(ctx) {
    var db = ctx.db; 
    var params = ctx.request.params;
    var where = {
        id: params.id
    } 
    return db.queryOne("select * from user", where);    
}
 

function withAddress(ctx) {
    var db = ctx.db;  
    var data = db.queryList("select * from user"); 
    db.linkQuery(data, {
        path: "AddrList",
        key: "id",

        sql: "select * from address",
        linkColumn: "user_id"
    });
    return data;  
}
 