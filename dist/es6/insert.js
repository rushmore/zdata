function add_user(ctx) {
    var db = ctx.db;   
    
    var user = {
        name: 'jack',
        fullname: 'Jack Jones',
        age: 48,
        remark: 'jack jack' 
    }
    db.insert("user", user);  
    
    return user;
}

function add_address(ctx) {
    var db = ctx.db;   
    
    var a = {
        user_id: 1,
        email: '44194462@qq.com', 
        city: 'ShenZhen'
    }
    db.insert("address", a);  
    return a;
}