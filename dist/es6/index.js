
function main(ctx) { 
	const resp = ctx.response;
	resp.setStatus(200);
	resp.setHeader("Content-Type", "text/html; charset=utf8")
	resp.setBody("<h1>home</h1>")
	
    return resp;
}
 
function params(ctx) { 
	var req = ctx.request;   
    return {
    	headers: req.headers,
    	params: req.params,
    	cookies: req.cookies,
    	body: req.body
    }
}