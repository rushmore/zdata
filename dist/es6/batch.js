function insert_user2(ctx) {
    var db = ctx.db;  
    
    var users = [];
    for(var i=0; i<10; i++){
    	var p = ['user'+i, 'User '+i, randInt(100) ]; 
    	users.push(p);
    }  
    var res = db.batchUpdate("INSERT INTO user(name, fullname, age) VALUES(?,?, ?) ",
         users); 
    return res;
}

function randInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

function insert_user_with_address(ctx){
    const db = ctx.db; 

    for(var i=0; i<10; i++){
        var user = {
            name: 'user'+i,
            fullname: 'User ' + i,
    		age: randInt(100)
        }
        db.insert('user', user);

        var addr = {
            email: 'user_'+i+"@qq.com",
            city: 'SZ',
            user_id: user.id
        };
        db.insert('address', addr);
    }
}

function insert_user(ctx) {
    var db = ctx.db;  
    
    var persons = [];
    for(var i=0; i<10; i++){
    	var p = {
            name: 'user'+i,
            fullname: 'User ' + i,
    		age: randInt(100)
    	}
    	persons.push(p);
    }
    
    var res = db.batchInsert("user", persons);  
    return res;
}


function insert_company(ctx) {
    const db = ctx.db;  
    
    let persons = [];
    for(var i=0; i<10; i++){
    	let p = {
            name: 'company_'+i,
            category: 'Cat'+(i%3),
    		list_date: '2018-11-'+(10+i)
    	}
    	persons.push(p);
    }
    
    var res = db.batchInsert("company", persons); 
    
    return res;
}

function insert_dept(ctx) {
    const db = ctx.db;  
    
    var comList = db.queryList('select id from company');
    var i = 0;
    for(var i in comList){
        var c = comList[i];
        var dept = {
            name: 'New_Dept_' + i, 
            company_id: c.id,
            manager_id: i,
        }
        db.insert('dept', dept);
    }
}