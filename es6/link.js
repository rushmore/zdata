
function user_address(ctx) {
    const db = ctx.db;

    var data = db.queryList("select * from user");

    db.linkQuery(data, {
        path: "AddrList",
        key: "id",

        sql: "select * from address",
        linkColumn: "user_id"
    });

    return data;
}

function user_roles(ctx) {
    var db = ctx.db;

    var data = db.queryList("select * from user");

    db.linkQuery(data, { //多对多
        path: "Roles",
        key: "id",
        sql: `select r.*, ur.user_id from role r, user_role ur 
        	 where r.id = ur.role_id`, 
        linkColumn: "ur.user_id"
    });

    db.linkQuery(data, { //再加多对一
        path: "Roles.Dept",
        key: "dept_id",
        sql: "select * from dept",
        linkColumn: "id",
        isOne: true
    });

    return data;
}

function company_list(ctx) {
    var db = ctx.db;

    var data = db.queryList("select * from company");

    db.linkQuery(data, {
        path: "Depts",
        key: "id",
        sql: "select * from dept",
        linkColumn: "company_id"
    });

    db.linkQuery(data, {
        path: "Depts.Manager",
        key: "id",
        sql: "select * from user",
        linkColumn: "id",
        isOne: true
    });

    return data;
}