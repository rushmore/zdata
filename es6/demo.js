
function params(ctx) { 
	var req = ctx.request;   
    return {
    	headers: req.headers,
    	params: req.params,
    	cookies: req.cookies,
    	body: req.body
    }
}
 