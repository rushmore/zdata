function context(ctx) { 
    const newCtx = {
        ...ctx,
        isJsCtx : true
    };
    return newCtx;
}

function configFilter(ctx) {
    return {
        //include: [],
        exclude: []
    };
}

function doFilter(ctx, next) { 
    //print(JSON.stringify(ctx.request));
    try {
        return next(ctx);  
    } catch (e) { 
        throw e;
    } finally { 
    	
    }
}