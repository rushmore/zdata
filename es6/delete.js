function by_id(ctx) {
    var db = ctx.db;  
    var req = ctx.request;
    var resp = ctx.response;
    
    var params = req.params;
    if(!params.id){
    	resp.status = 400;
    	resp.body = "Missing id in query";
    	return resp;
    } 
    
    var res = db.execute("delete from user where id=?", [params.id]);  
    return res;
}