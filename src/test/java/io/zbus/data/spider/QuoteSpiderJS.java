package io.zbus.data.spider;

public class QuoteSpiderJS {
	
	public static void main(String[] args) throws Exception {
		Spider spider = new Spider(); 
		spider.setUrlSeed("http://quotes.toscrape.com/page/1/");
		spider.setJsHandler("quotes.js");    // handled by js
		
		spider.start();
	}
}
