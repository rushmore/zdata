package io.zbus.data.spider;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpiderSpring {
	
	@SuppressWarnings("resource")
	public static void main(String[] args) throws Exception {
		new ClassPathXmlApplicationContext("spider.xml");    
	}
}
