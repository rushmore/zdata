package io.zbus.data.spider;

import java.util.Arrays;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class QuoteSpiderJava {  
	public static void main(String[] args) throws Exception {
		Spider spider = new Spider();
		
		spider.setUrlSeed("http://quotes.toscrape.com/page/1/");
		
		spider.setHandler((doc, ctx) -> {  //handled by java
			
			Elements all = doc.select("div.quote");
			for (Element e : all) {
				String quote = e.selectFirst("span.text").text();
				String author = e.selectFirst("small.author").text();
				System.out.println(quote + "\n--" + author);
			}

			String baseUrl = (String)ctx.get("baseUrl");
			Element e = doc.selectFirst("nav li.next > a");
			if(e != null){ 
				String url = baseUrl + e.attr("href");
				return Arrays.asList(url);
			}  
			
			return null; 
		});

		spider.start();
	}
}
