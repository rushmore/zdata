package io.zbus.data.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;

import io.zbus.data.api.Db;
import io.zbus.data.api.Db.LinkQuery;
import io.zbus.data.kit.JsonKit; 

@Controller
public class JdbcDbExample {
	@Autowired
	private Db db;
	
	public void test() { 
		long start = System.currentTimeMillis();   
		
		List<User> data = db.queryList(User.class,  "select * from user where age>?", new Object[] {30});    
		
		LinkQuery query = new LinkQuery();
		query.path = "addrList";
		query.key = "id";  
		
		query.sql = "select * from address";
		query.linkColumn = "user_id";
		
		
		
		db.linkQuery(data, query);
		
		long end = System.currentTimeMillis(); 
		System.out.println(end-start);
		
		System.out.println(JsonKit.toJSONPrintString(data));
	}
	 

	@SuppressWarnings("resource")
	public static void main(String[] args) throws Exception {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("spring-processor.xml");
		JdbcDbExample example = ctx.getBean(JdbcDbExample.class);
		//for(int i=0;i<100;i++)
		example.test();
		
		//User user = new User();
		//TableKit.set(user, "addrList", null);
	} 
}
