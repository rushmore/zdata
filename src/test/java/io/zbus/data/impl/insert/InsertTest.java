package io.zbus.data.impl.insert;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;

import io.zbus.data.api.Db;
import io.zbus.data.impl.User; 

@Controller
public class InsertTest {
	@Autowired
	private Db db;
	
	@SuppressWarnings("resource")
	@Before
	public void setup() {
		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("spring-processor.xml"); 
		db = ctx.getBean(Db.class);
	}
	 
	
	@Test
	public void save() { 
		User user = new User();
		user.id = 1;
		user.name = "test111";
		user.age = 11; 
		
		db.save("user", user);   
	}   
}
