package io.zbus.data.impl.meta;

import java.sql.Connection;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.support.JdbcUtils;
import org.springframework.stereotype.Controller;
 

@Controller
public class MetaDataExample {
	@Autowired
	private DataSource dataSource;
	
	public MetaData reflect() throws Exception { 
		Connection conn = null;
		try {
			conn = dataSource.getConnection(); 
			return MetaReader.reflect(conn);
		} finally {
			JdbcUtils.closeConnection(conn);
		} 
	}

	 

	@SuppressWarnings("resource")
	public static void main(String[] args) throws Exception {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("spring-processor.xml");
		MetaDataExample example = ctx.getBean(MetaDataExample.class);
		MetaData db = example.reflect();
		System.out.println(db.tables.get("user"));
	}

}
