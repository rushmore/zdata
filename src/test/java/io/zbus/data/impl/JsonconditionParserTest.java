package io.zbus.data.impl;

import java.util.ArrayList;
import java.util.List;

import io.zbus.data.kit.JsonKit;
import io.zbus.kit.FileKit;

public class JsonconditionParserTest {

	public static void main(String[] args) throws Exception { 
		FileKit kit = new FileKit();
		String where = kit.loadFile("where2.js");
		Object json = JsonKit.parse(where);
		
		JsonConditionParser parser = new JsonConditionParser();
		List<Object> paramList = new ArrayList<>();
		String res = parser.parse(json, paramList);
		System.out.println(res);
		System.out.println(paramList);
	} 
}
