package io.zbus.data.impl.kit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import io.zbus.data.impl.TableKit;
import io.zbus.kit.StrKit;

public class TableKitTest {
	
	@Test
	public void test_whereExists() { 
		Assert.assertTrue(TableKit.whereExists(" wheRe "));
		Assert.assertTrue(TableKit.whereExists("a wheRe id=1"));
		Assert.assertTrue(TableKit.whereExists("a wHeRe\nxy"));
		Assert.assertFalse(TableKit.whereExists(" WHERE")); 
	}
	  
	
	private Map<String, Object> buildMap(String name, Map<String, Object> subLayer, int len){
		Map<String, Object> layer = new HashMap<>();
		String key = StrKit.uuid(); 
		layer.put(key+"-p1", "x1");
		layer.put(key+"-p2", "x2");
		layer.put("testKey", StrKit.uuid());
		if(len > 0 && subLayer != null) {
			List<Map<String, Object>> arr = new ArrayList<>();
			for(int i=0;i<len;i++) arr.add(new HashMap<>(subLayer));
			if(len>1) layer.put(name, arr);
			else layer.put(name, subLayer);
		}
		return layer;
	}
	
	@Test
	public void test_findByPath() { 
		
		Map<String, Object> layer0 = buildMap(null, null, 0);
		Map<String, Object> layer1 = buildMap("Options", layer0, 2);  
		Map<String, Object> layer2 = buildMap("Questions", layer1, 3);  
		Map<String, Object> layer3 = buildMap("Root", layer2, 2);   
		
		//System.out.println(JsonKit.toJSONPrintString(layer2));
		List<Object> dataList = new ArrayList<>();
		dataList.add(layer3);
		String[] paths = "Root.Questions.Options.testKey".split("[.]");
		List<Object> res = new ArrayList<>();
		TableKit.findByPath(res, dataList, paths, 0); 
		//System.out.println(JsonKit.toJSONPrintString(res));
		Assert.assertTrue(res.size() == 2*3*2);
	}
	 
	@Test
	public void test_findKeys() { 
		Map<String, Object> layer0 = buildMap(null, null, 0);
		Map<String, Object> layer1 = buildMap("Options", layer0, 2);  
		Map<String, Object> layer2 = buildMap("Questions", layer1, 3);  
		Map<String, Object> layer3 = buildMap("Root", layer2, 2);   
		
		//System.out.println(JsonKit.toJSONPrintString(layer2));
		List<Object> dataList = new ArrayList<>();
		dataList.add(layer3);
		Set<String> res = TableKit.findKeys(dataList, "Root.Questions.Options.testKey".split("[.]"));  
		Assert.assertTrue(res.size()==1);
		//System.out.println(res);
	}
}
