package io.zbus.data.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;

import io.zbus.data.api.Db;
 

@Controller
public class TransactionExample {
	@Autowired
	private Db db;
	
	public void test() {
		 db.tx(()->{
			db.execute("insert into test_person(name, age) values(?,?)", new Object[] {"rush", 22 });
			//throw new IllegalStateException();
		 });
	}
	 

	@SuppressWarnings("resource")
	public static void main(String[] args) throws Exception {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("spring-processor.xml");
		TransactionExample example = ctx.getBean(TransactionExample.class); 
		example.test();
	}

}
