package io.zbus.data.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;

import io.zbus.data.api.Db; 

@Controller
public class SaveExample {
	@Autowired
	private Db db;
	
	public void test() {   
		Map<String, Object> user = new HashMap<>();
		user.put("id", 1);
		user.put("name", "rushmore_testxx");
		user.put("fullname", "Hong Leiming");
		user.put("age", 22); 
		
		db.save("user", user);   
	}
	 

	@SuppressWarnings("resource")
	public static void main(String[] args) throws Exception {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("spring-processor.xml");
		SaveExample example = ctx.getBean(SaveExample.class);
		//for(int i=0;i<100;i++)
		example.test();
	}

}
