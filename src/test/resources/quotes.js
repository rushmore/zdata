
function main(doc, ctx) {   //jsoup syntax
	var db = ctx.db;
	var all = doc.select('div.quote'); 
	var res = [];
	for(var i in all){
		var e = all[i];
		var quote = e.selectFirst("span.text").text();
		var author = e.selectFirst("small.author").text();
		print(quote + "\n--" + author);
		var q = {
			quote: quote, 
			author: author
		}  
		res.push(q);
	}
	
	if(res.length > 0) {
		//db.batchInsert('quote', res);
	}
	
	var e = doc.selectFirst("nav li.next > a");
	if(e){ 
		return [ctx.baseUrl + e.attr("href")];
	} 
} 
 