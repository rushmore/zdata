package io.zbus.data.server.js;

import io.zbus.transport.Message;

public interface FuncInvoker {

	Object invoke(Message req, Message res) throws Exception;
	
}
