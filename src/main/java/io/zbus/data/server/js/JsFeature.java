package io.zbus.data.server.js;

import java.util.List;

public interface JsFeature {

	List<String> methods();
	Object callMethod(String name, Object... args) throws Exception;
	
}
