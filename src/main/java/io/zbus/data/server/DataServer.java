package io.zbus.data.server;

import io.zbus.rpc.Spring;

public class DataServer { 
 
	public static void main(String[] args) throws Exception { 
        String activeProfile = System.getProperty("env", "local");
        System.setProperty("spring.profiles.active", activeProfile);  
        Spring.run("spring-context.xml"); //load spring inside and do preparation for zbus 
	}

}
