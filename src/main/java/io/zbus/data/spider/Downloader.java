package io.zbus.data.spider;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;
import okhttp3.Request;
import okhttp3.Response;

public class Downloader {
	static Downloader INSTANCE = new Downloader();
	
	OkHttpClient.Builder builder = new Builder();  
	OkHttpClient client = builder.build();  
	
	public Document download(String url) throws IOException { 
		Request request = new Request.Builder()
				.url(url)
				.header("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36")
				.build();
		
		Response res = client.newCall(request).execute(); 
		
		Document doc = Jsoup.parse(res.body().string());
		res.close();
		return doc;
	}
	
	public static Document get(String url) throws IOException {  
		return INSTANCE.download(url);
	} 
}
