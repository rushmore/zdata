package io.zbus.data.spider;

import java.util.List;
import java.util.Map;

import org.jsoup.nodes.Document;

public interface SpiderHandler {
	List<String> handle(Document doc, Map<String, Object> ctx);
}
