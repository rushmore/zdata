package io.zbus.data.impl.dialect;

public interface SqlDialect {
	
	public String resolve(String word); 
	
	public String paging(String sql, int offset, int limit);
}
