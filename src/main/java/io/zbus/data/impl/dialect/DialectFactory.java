package io.zbus.data.impl.dialect;

import io.zbus.data.kit.StrKit;

public class DialectFactory {
	public static SqlDialect fromProductName(String producerName) {
		producerName = producerName.toLowerCase();
		
		if(producerName.startsWith("mysql")) {
			return new MySqlDialect();
		} else if(producerName.startsWith("microsoft")) {
			return new SqlServerDialect();
		} else if(producerName.startsWith("oracle")) {
			return new OracleDialect();
		} else if(producerName.startsWith("postgresql")) {
			return new PgDialect();
		} else {
			return new EmptyDialect();
		}
	}
	
	public static class EmptyDialect implements SqlDialect { 
		@Override
		public String resolve(String word) { 
			return word;
		}

		@Override
		public String paging(String sql, int offset, int limit) { 
			return sql;
		}  
		
	}
	
	public static class MySqlDialect implements SqlDialect { 
		@Override
		public String resolve(String word) { 
			return StrKit.isEmpty(word) ? "" : "`"+word+"`";
		}

		@Override
		public String paging(String sql, int offset, int limit) { 
			return sql + String.format(" LIMIT %d, %d", offset, limit);
		}
	}

	public static class OracleDialect implements SqlDialect { 
		@Override
		public String resolve(String word) { 
			return StrKit.isEmpty(word) ? "" : "\""+word+"\"";
		} 
		
		@Override
		public String paging(String sql, int offset, int limit) { 
			return sql;
		} 
	}

	public static class PgDialect implements SqlDialect { 
		@Override
		public String resolve(String word) { 
			return StrKit.isEmpty(word) ? "" : "\""+word+"\"";
		} 
		
		@Override
		public String paging(String sql, int offset, int limit) { 
			return sql;
		} 
	}

	public static class SqlServerDialect implements SqlDialect { 
		@Override
		public String resolve(String word) { 
			return StrKit.isEmpty(word) ? "" : "["+word+"]";
		} 
		
		@Override
		public String paging(String sql, int offset, int limit) { 
			return sql + String.format(" OFFSET %d ROWS FETCH NEXT %d ROWS ONLY", offset, limit);
		} 
	} 

}
