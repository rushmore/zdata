package io.zbus.data.impl.meta;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;

public class PrimaryKey { 
	public String tableCat;  
	public String tableSchem;
	public String tableName; 
	public String columnName;
	public Integer keySeq;;
	public String pkName; 
	@Override
	public String toString() {
		return JSON.toJSONString(this,  
				SerializerFeature.PrettyFormat,
				SerializerFeature.WriteMapNullValue
		); 
	}
}
