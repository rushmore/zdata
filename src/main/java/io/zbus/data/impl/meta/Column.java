package io.zbus.data.impl.meta;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;

public class Column { 
	public String scopeTable;
	public String tableCat;
	public Integer bufferLength;
	public Boolean isNullable; //YES/NO
	public String tableName;
	public String columnDef;
	public String scopeCatalog;
	public String tableSchem;
	public String columnName;
	public Boolean nullable;;
	public String remarks;
	public String decimalDigits;
	public String numPrecRadix;
	public Integer sqlDatetimeSub;
	public Boolean isGeneratedColumn;
	public Boolean isAutoincrement;
	public Integer sqlDataType;
	public Integer charOctetLength;
	public Integer ordinalPosition;
	public String scopeSchema;
	public String sourceDataType;
	public Integer dataType;
	public String typeName;
	public Integer columnSize;
	
	@Override
	public String toString() {
		return JSON.toJSONString(this,  
				SerializerFeature.PrettyFormat,
				SerializerFeature.WriteMapNullValue
		); 
	}
}
