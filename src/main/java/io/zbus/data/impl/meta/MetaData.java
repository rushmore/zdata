package io.zbus.data.impl.meta;

import java.util.HashMap;
import java.util.Map;

import io.zbus.data.impl.dialect.SqlDialect;

public class MetaData {
	public Map<String, Table> tables = new HashMap<>();
	public String productName;
	public String productVersion;
	
	public SqlDialect dialect;
}
