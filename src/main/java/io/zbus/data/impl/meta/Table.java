package io.zbus.data.impl.meta;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;

public class Table {
	public String tableCat;
	public String tableName;
	public String selfReferenceColName; 
	public String typeSchem;
	public String typeCat;
	public String tableType;
	public String remarks;
	public String refGeneration;
	public String typeName;
	
	public List<Column> columnsInSeq = new ArrayList<>();
	public Map<String, Column> columns = new TreeMap<String, Column>(String.CASE_INSENSITIVE_ORDER);
	public Map<String, PrimaryKey> primaryKeys = new TreeMap<String, PrimaryKey>(String.CASE_INSENSITIVE_ORDER);

	public Column autoColumn;

	@Override
	public String toString() { 
		return JSON.toJSONString(this,  
				SerializerFeature.PrettyFormat,
				SerializerFeature.WriteMapNullValue
		); 
	}
}
